package fr.cylpios.potamis;

import java.util.Date;
import java.util.List;

public class Plant {
    private  String name;
    private  String uri;
    private  String ico_uri;

    private Date sowing_period_start;
    private Date sowing_period_end;
    private Date harvesting_period_start;
    private Date harvesting_period_end;

    //If Date are to hard to work with;
    private String sowing_periode;
    private String harvesting_period;

    //Should be in day
    private Integer spray_periodicity;

    //Should be in meter
    private Integer sowing_depth;
    private Integer sowing_distance;

    //Need to be in a defined list
    private String soil_type;

    //Should be in Celsius
    private Integer ideal_temperature;

    //Should be in Lumens
    private Integer ideal_illumination;

    private List<Plant> associated_plants;
    private Advice advice;

    public Plant(String name, String uri, String ico_uri, Advice advice) {
        this.name = name;
        this.uri = uri;
        this.uri = ico_uri;
        this.advice = advice;
    }

    public Plant(String name, String uri, String ico_uri, String sowing_periode, String harvesting_period, Integer spray_periodicity, Integer sowing_depth, Integer sowing_distance) {
        this.name = name;
        this.uri = uri;
        this.ico_uri = ico_uri;
        this.sowing_periode = sowing_periode;
        this.harvesting_period = harvesting_period;
        this.spray_periodicity = spray_periodicity;
        this.sowing_depth = sowing_depth;
        this.sowing_distance = sowing_distance;
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }

    public String getSowing_periode() {
        return sowing_periode;
    }

    public String getHarvesting_period() {
        return harvesting_period;
    }

    public Integer getSpray_periodicity() {
        return spray_periodicity;
    }

    public Integer getSowing_depth() {
        return sowing_depth;
    }

    public Integer getSowing_distance() {
        return sowing_distance;
    }

    public String getSoil_type() {
        return soil_type;
    }

    public Integer getIdeal_temperature() {
        return ideal_temperature;
    }

    public Integer getIdeal_illumination() {
        return ideal_illumination;
    }

    public List<Plant> getAssociated_plants() {
        return associated_plants;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void addAssociated_plant(Plant plant) {
        this.associated_plants.add(plant);
    }
}
