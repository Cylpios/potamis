package fr.cylpios.potamis.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import fr.cylpios.potamis.Action;
import fr.cylpios.potamis.ActionsInflater;
import fr.cylpios.potamis.R;

import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActionFragment extends Fragment {


    JSONObject obj;
    private int lastpos = -1;
    ArrayAdapter itemAdapter;
    ViewGroup container;
    LayoutInflater inflater;
    Bundle savedInstance;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        this.inflater = inflater;
        this.container = container;
        this.savedInstance = savedInstanceState;

        ViewGroup rootView = (ViewGroup)inflater.inflate(R.layout.fragment_action, container, false);
            /*
        List<Actions> =

        ListView listView = (ListView)rootView.findViewById(R.id.action_Linear);
        ActionsInflater actionsInflater = new ActionsInflater(getActivity(), )
        listView.setAdapter(ActionsInflater):*/
        return rootView;
    }





    //Open a particular part of JsonFile
    public JSONArray OpenJson(String array){
        JSONArray m_jArry = null;
        try {
            if(obj == null){
                obj = new JSONObject(loadJSONFromAsset());
                m_jArry = obj.getJSONArray(array);
            }else{
                m_jArry = obj.getJSONArray(array);
            }
            return m_jArry;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return m_jArry;
    }

    //Get JSON File
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("actions.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
