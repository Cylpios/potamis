package fr.cylpios.potamis;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class MenuInflater extends ArrayAdapter<MenuItem> {

    public Activity context;
    public List<MenuItem> menuItems;


    public MenuInflater(Activity context, List<MenuItem> menuItems) {
        super(context, R.layout.plant_layout,menuItems);
        this.menuItems = menuItems;
        this.context = context;
    }


    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //init
        LayoutInflater inflater = context.getLayoutInflater();
        MenuItem menuItem = menuItems.get(position);
        View listViewItem = inflater.inflate(R.layout.menu_layout, null, true);

        ImageView menu_img = (ImageView) listViewItem.findViewById(R.id.menu_img);
        menu_img.setContentDescription("Catégorie " + menuItem.getName());

        int resID = context.getResources().getIdentifier(menuItem.getUri() , "drawable", context.getPackageName());
        menu_img.setImageResource(resID);
        Log.d("Details", "getView: " + String.valueOf(position));
        return listViewItem;
    }
}
