package fr.cylpios.potamis;

import java.util.Date;

public class Action {
    private  User user;
    private  Garden garden;

    private  Date created;
    private  Integer points;
    private  String description;

    private String action_type;

    public Action(User user, Garden garden, Integer points, Date created, String description) {
        this.user = user;
        this.garden = garden;
        this.points = points;
        this.created = created;
        this.description = description;
        this.action_type = "default";
    }

    public User getUser() {
        return user;
    }

    public Garden getGarden() {
        return garden;
    }

    public Integer getPoints() {
        return points;
    }

    public Date getCreated() {
        return created;
    }

    public String getDescription() {
        return description;
    }

    public String getAction_type() {
        return action_type;
    }
}
