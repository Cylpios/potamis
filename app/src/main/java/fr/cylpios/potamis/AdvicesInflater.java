package fr.cylpios.potamis;

import android.app.Activity;
import android.widget.ArrayAdapter;

import java.util.List;

public class AdvicesInflater extends ArrayAdapter<Advice> {

    private Activity context;
    private List<Advice> advices;
    private Advice advice;

    public AdvicesInflater(Activity context, List<Advice> advices) {
        super(context, R.layout.action_layout,advices);
        this.advices = advices;
        this.context = context;
    }
}