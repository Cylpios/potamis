package fr.cylpios.potamis;

public class MenuItem {
    private  int id;
    private  String name;
    private  String uri;

    public MenuItem(){}

    //Getter
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getUri() {
        return uri;
    }


    //Setter
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
