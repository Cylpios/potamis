package fr.cylpios.potamis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.transition.Slide;
import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.cylpios.potamis.fragments.ActionFragment;
import fr.cylpios.potamis.fragments.AdviceFragment;
import fr.cylpios.potamis.fragments.MapFragment;

public class MainActivity extends AppCompatActivity {


    JSONObject obj;
    private int lastpos = -1;
    ArrayAdapter itemAdapter;

    private ViewPager pager;
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pager = findViewById(R.id.viewpager);

        InflateMenu();
    }



    @Override
    public void onStart(){
        super.onStart();

        List<Fragment> list = new   ArrayList<>();
        list.add(new ActionFragment());
        list.add(new AdviceFragment());
        list.add(new MapFragment());

        pagerAdapter = new SlideAdapter(getSupportFragmentManager(), list);
        pager.setAdapter(pagerAdapter);
    }




    public void InflateMenu(){
        List<MenuItem> menuItems = getMenu();
        ListView listViewMenu = (ListView) findViewById(R.id.menu_lv);
        listViewMenu.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        ArrayAdapter menuAdapter = new MenuInflater(MainActivity.this, menuItems);
        listViewMenu.setAdapter(menuAdapter);


        listViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position != lastpos){
                    pager.setCurrentItem( position );
                }
                parent.setSelection(position);
                view.setSelected(true);
                Log.d("Details", "onItemClick: ");
            }
        });

        listViewMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Details", "onItemSelected: ");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        listViewMenu.setItemChecked(0, true);
    }



    public List<MenuItem> getMenu(){
        List<MenuItem> menuItems = new ArrayList<>();
        try{
            JSONArray m_jArry = OpenJson("menuItems");
            if(m_jArry != null){
                for (int i = 0; i < m_jArry.length(); i++) {
                    MenuItem menuItem = new MenuItem();

                    JSONObject jo_inside = m_jArry.getJSONObject(i);

                    menuItem.setId(jo_inside.getInt("id"));
                    menuItem.setName(jo_inside.getString("nom"));
                    menuItem.setUri(jo_inside.getString("uri"));

                    menuItems.add(menuItem);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return menuItems;
    }



    //Open a particular part of JsonFile
    public JSONArray OpenJson(String array){
        JSONArray m_jArry = null;
        try {
            if(obj == null){
                obj = new JSONObject(loadJSONFromAsset());
                m_jArry = obj.getJSONArray(array);
            }else{
                m_jArry = obj.getJSONArray(array);
            }
            return m_jArry;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return m_jArry;
    }

    //Get JSON File
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("items.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
