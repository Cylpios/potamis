package fr.cylpios.potamis;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PlantsInflater extends ArrayAdapter<Plant> {

    private Activity context;
    private List<Plant> plants;
    private Plant plant;

    public PlantsInflater(Activity context, List<Plant> plants) {
        super(context, R.layout.action_layout,plants);
        this.plants = plants;
        this.context = context;
    }
}