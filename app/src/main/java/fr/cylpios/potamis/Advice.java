package fr.cylpios.potamis;

import java.util.Date;

public class Advice {
    private  Date created;
    private  String author;
    private  String text;

    public Advice(Date created, String author, String text, String uri) {
        this.created = created;
        this.author = author;
        this.text = text;
    }
    public Date getCreated() {
        return created;
    }

    public String getAuthor() {
        return author;
    }

    public String getText() {
        return text;
    }
}
