package fr.cylpios.potamis;

public class User {
    private  String username;
    private  String mail;

    private  String name;
    private  String lname;

    private  Integer points;

    public User(String username, String mail) {
        this.username = username;
        this.mail = mail;
    }

    public User(String username, String mail, String name, String lname) {
        this.name = name;
        this.lname = lname;
        this.username = username;
        this.mail = mail;
        this.points = 0;
    }

    public String getUsername() {
        return username;
    }

    public String getMail() {
        return mail;
    }

    public String getName() {
        return name;
    }

    public String getLname() {
        return lname;
    }

    public Integer getPoints() {
        return points;
    }

    public void addPoints(Integer points) {
        this.points += points;
    }
}
