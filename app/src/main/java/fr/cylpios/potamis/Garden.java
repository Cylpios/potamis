package fr.cylpios.potamis;

import java.lang.reflect.Array;
import java.util.List;

public class Garden {
    private  String name;
    private  String adress;
    private  String author;

    private  Integer longitude;
    private  Integer latitude;

    private List<Plant> plants;
    private List<User> users;
    private List<Action> actions;


    public Garden(String name, String adress, String author, Integer longitude, Integer latitude) {
        this.name = name;
        this.adress = adress;
        this.author = author;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

    public String getAuthor() {
        return author;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public Integer getLatitude() {
        return latitude;
    }
}
