package fr.cylpios.potamis;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ActionsInflater extends ArrayAdapter<Action> {

    private Activity context;
    private List<Action> actions;
    private Action action;

    public ActionsInflater(Activity context, List<Action> actions) {
        super(context, R.layout.action_layout,actions);
        this.actions = actions;
        this.context = context;
    }
}
